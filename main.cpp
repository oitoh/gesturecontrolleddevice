#include <iostream>
#include <algorithm>
#include <numeric>
#include <utility>
#include <cmath>
#include <cstdint>

#include <opencv/cv.hpp>
#include <opencv/highgui.h>

#include <boost/filesystem.hpp>

using namespace std;

namespace fs = boost::filesystem;

list<fs::path> listFiles(string path) {
    list<fs::path> files;
    fs::path dir(path);

    for(fs::directory_iterator entry(dir); entry != fs::directory_iterator(); ++entry) {
        if(fs::is_regular_file(entry->status()))
            files.push_back(entry->path().native());
    }

    return files;
}

vector<cv::Mat> loadFrames(list<fs::path>& files) {
    vector<cv::Mat> frames(files.size());

    int i = 0;
    for(auto file: files) {
        frames[i] = cv::imread(file.native());
        ++i;
    }

    return frames;
}

vector<cv::Mat> recodeToSingleChannel(vector<cv::Mat>& raw) {
    vector<cv::Mat> reencoded;
    reencoded.reserve(raw.size());

    cv::Mat rFrame;
    int nChannels = raw[0].channels();
    int nRows = raw[0].rows;
    int nCols = raw[0].cols * nChannels;
    uchar *p;

    for(cv::Mat& frame: raw) {
        rFrame = cv::Mat(frame.size(), CV_32FC1);
        for(int i = 0; i < nRows; ++i) {
            p = frame.ptr(i);
            for(int j = 1; j < nCols; j += 3) {
                rFrame.at<float>(i, j / 3) = (float(p[j]) + (float(p[j+1]) * 256.f))/4095.f;
            }
        }
        reencoded.push_back(rFrame);
    }

    return reencoded;
}

vector<int> calcSpatialHist(cv::Mat img) {
    const int binNum = 32;
    const int binSize = img.cols / binNum;

    vector<int> histogram(binNum, 0);

    int count;
    for(int i = 0; i < binNum; ++i) {
        count = 0;
        for(int j = i * binSize; j < (i + 1) * binSize; ++j) {
            for(int k = 0; k < img.rows; ++k) {
                if(img.at<float>(k, j) > 0.f)
                    ++count;
            }
        }
        histogram[i] = count;
    }

    return histogram;
}

void writeToFile(string filename, vector<cv::Mat>& frames) {
     cv::VideoWriter writer(filename, cv::VideoWriter::fourcc('D', 'I', 'V', 'X'), 25.0, frames[0].size(), frames[0].type() != CV_32FC1);

     cv::Mat buf;
     for(cv::Mat& frame: frames) {
         if (frame.type() == CV_32FC1) {
            frame.convertTo(buf, CV_8U, 255.0);
            writer.write(buf);
         }
         else
            writer.write(frame);
     }
}

void removeNoise(cv::Mat& frame, cv::Mat& noise, cv::Mat& dst) {
    cv::multiply(noise, frame, dst);
    cv::morphologyEx(dst, dst,
        cv::MORPH_OPEN,
        cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3))
    );
}

void updateNoiseModel(cv::Mat& noise, cv::Mat& frame) {
    cv::morphologyEx(frame, noise, cv::MORPH_DILATE,
        cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5)),
        cv::Point(-1, -1), 2
    );
    cv::GaussianBlur(noise, noise, cv::Size(5, 5), 1.0);
    cv::threshold(noise, noise, 0.0, 1.0, cv::THRESH_BINARY_INV);
}

int main(int argc, char *argv[])
{
    /* Carregar frames a partir dos arquivos */
    auto files = listFiles(argc > 1 ? "res/log1/" : "res/log2/");
    files.sort();
    vector<cv::Mat> frames = loadFrames(files);
    writeToFile("original.mkv", frames);

    /* Recodificar para 12 bits de profundidade */
    vector<cv::Mat> reencoded = recodeToSingleChannel(frames);
    writeToFile("recodificado.mkv", reencoded);

    /* Remover frente e fundo */
    vector<cv::Mat>::size_type currentFrame;
    for(currentFrame = 0; currentFrame < reencoded.size(); ++currentFrame) {
        cv::threshold( // Limiar para fundo
            reencoded[currentFrame], reencoded[currentFrame],
            7.5f * 256.f / 4095.f, 0.0, cv::THRESH_TOZERO_INV
        );
        cv::threshold( // Limiar para frente
            reencoded[currentFrame], reencoded[currentFrame],
            6.25f * 256.f / 4095.f, 0.0, cv::THRESH_TOZERO
        );
    }
    writeToFile("extraido.mkv", reencoded);

    /* Modelar resíduo da extração */
    cv::Mat buf;
    cv::Mat noise = cv::Mat::zeros(reencoded[0].size(), reencoded[0].type());
    for(currentFrame = 0; currentFrame < 15; ++currentFrame) {
        cv::morphologyEx(reencoded[currentFrame], buf, cv::MORPH_DILATE,
            cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
            cv::Point(-1, -1), 2
        );
        noise += buf;
    }
    cv::GaussianBlur(noise, noise, cv::Size(3, 3), 1.0);
    cv::threshold(noise, noise, 0.0, 1.0, cv::THRESH_BINARY_INV);
    noise.convertTo(buf, CV_8U, 255.0);
    cv::imwrite("extractionNoise.jpg", buf);

    /* Remover resíduo da extração */
    for(currentFrame = 0; currentFrame < reencoded.size(); ++currentFrame) {
        removeNoise(reencoded[currentFrame], noise, reencoded[currentFrame]);
    }

    enum STATE {
        STATE_INIT,
        STATE_UNSTABLE,
        STATE_STABLE,
        STATE_TRACK
    } currentState = STATE_INIT;

    int64_t flow = 0;

    struct {
        vector<int>::size_type reach;
        int steadyCount;
        vector<int>::size_type index;
        vector<int> flowHist;
        vector<int>::iterator it;
    } controller;

    /* Gerar histograma com a distribuição de pontos ao longo da horizontal na cena */
    vector<int> backgroundHist(calcSpatialHist(reencoded[0]));
    vector<int> hist(backgroundHist.size(), 0);
    vector<int> diffHist(backgroundHist.size(), 0);

    auto is_above_threshold_f = [](int it){ return it > 40; };
    noise = cv::Mat::ones(reencoded[0].size(), CV_32FC1);
    for(currentFrame = 1; currentFrame < reencoded.size(); ++currentFrame) {
        /* Encontrar diferenças entre o histograma de fundo e atual */
        removeNoise(reencoded[currentFrame], noise, buf);
        hist = calcSpatialHist(buf);
        for(vector<int>::size_type i = 0; i < hist.size(); ++i) {
            diffHist[i] = hist[i] - backgroundHist[i];
        }

        switch(currentState) {
        case STATE_INIT:
            /* Aguarda o individuo entrar na cena */
            if(std::any_of(diffHist.begin(), diffHist.end(), is_above_threshold_f)) {
                controller.steadyCount = 0;
                currentState = STATE_UNSTABLE;
                cout << "Instável" << endl;
            }
            backgroundHist.swap(hist);
        break;

        case STATE_UNSTABLE:
            /* Aguarda o individuo se posicionar na cena */

            /* Se nenhum estiver acima do threshold durante um dado numero de frames, a imagem está estável */
            if(!std::any_of(diffHist.begin(), diffHist.end(), is_above_threshold_f)) {
                ++controller.steadyCount;
            }
            else
                controller.steadyCount = 0;

            backgroundHist.swap(hist);

            /* Se a imagem está estável, usa modelo atual como modelo de ruído */
            if(controller.steadyCount == 8) {
                updateNoiseModel(noise, reencoded[currentFrame]);
                currentState = STATE_STABLE;
                cout << "Estável" << endl;
            }
        break;

        case STATE_STABLE:
            /* Buscar variações intensas entre histogramas */
            controller.it = std::find_if(diffHist.begin(), diffHist.end(), is_above_threshold_f);
            if(controller.it != diffHist.end()) {
                controller.index = controller.it - diffHist.begin();
                controller.flowHist = vector<int>(diffHist.size(), 0);
                controller.steadyCount = 0;

                currentState = STATE_TRACK;
                cout << "Rastreio iniciado" << endl;
            }
            else
                backgroundHist.swap(hist);
        break;

        case STATE_TRACK:
            /* Se nenhum estiver acima do threshold durante um dado numero de frames, o movimento acabou */
            if(!std::any_of(diffHist.begin(), diffHist.end(), is_above_threshold_f)) {
                ++controller.steadyCount;
                backgroundHist.swap(hist);
            }
            else {
                controller.steadyCount = 0;
                for(vector<int>::size_type i = 0; i < diffHist.size(); ++i)
                    controller.flowHist[i] += diffHist[i];
            }
            if(controller.steadyCount == 8){
                controller.reach = std::min(controller.index, diffHist.size() - controller.index - 1);
                controller.it = controller.flowHist.begin() + controller.index;
                flow = std::accumulate(
                    controller.it,
                    controller.it + controller.reach + 1,
                    0
                );
                flow -= std::accumulate(
                    controller.it - controller.reach,
                    controller.it,
                    0
                );
                cout << "Fluxo: " << to_string(flow);
                if(flow < -500)
                    cout << " - Anterior" << endl;
                else if(flow > 500)
                    cout << " - Próximo" << endl;
                else
                    cout << " - Ignorado" << endl;

                backgroundHist.swap(hist);
                updateNoiseModel(noise, reencoded[currentFrame]);

                currentState = STATE_STABLE;
            }
        break;
        }
        cv::imshow("Resultado", buf);
        cv::waitKey();
    }

    writeToFile("resultado.mkv", reencoded);

    clog << endl;
    cout << endl;

    return 0;
}
