## Descrição em português
Programa em OpenCV para processar imagens obtidas por um Kinect, simulando o comportamento de um sensor de gestos de um dispositivo que muda a página de uma apresentação de slides.

## English description
OpenCV application to process images from a Kinect, detecting gestures from an individual to simulate the behaviour of a gesture-controlled device to turn slide presentation pages.